/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erickcabral.firebase.firebase;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.security.token.TokenGenerator;
import com.firebase.security.token.TokenOptions;
import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreOptions;
import com.google.cloud.firestore.WriteResult;
import com.google.cloud.storage.Bucket;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.UserRecord;
import com.google.firebase.cloud.StorageClient;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Erick Cabral
 */
public class FirebaseApi {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) throws InterruptedException, FirebaseAuthException, IOException, Exception {

		String servicePath = "F:\\Cursos\\JAVA SE\\Bibliotecas\\Chave Firebase HCT\\FireCredential.json";
		String projectUrl = "https://hypertheory-fd947.firebaseio.com";
		String userURL = "https://hypertheory-fd947.firebaseio.com/users";
		Firebase firebase;// = new Firebase(projectUrl);

		FileInputStream serviceStream = null;
		GoogleCredentials credential = null;

		FirebaseApp fireApp = null;
		FirebaseOptions options = null;
		FirestoreOptions firestoreOptions = null;
		HashMap<String, String> dataToInput = new HashMap<>();

		Firestore firestoreDb = null;

		Firebase.AuthResultHandler authResultHandler = new Firebase.AuthResultHandler() {
			@Override
			public void onAuthenticated(AuthData ad) {
				System.out.println("AUTH SUCCESS - > " + ad);
				System.out.println("AUTH SUCCESS - > " + ad.getProvider());
				System.out.println("AUTH SUCCESS - > " + ad.getUid());
				System.out.println("AUTH SUCCESS - > " + ad.getAuth());
			}

			@Override
			public void onAuthenticationError(FirebaseError fe) {
				System.out.println("AUTH ERROR -> " + fe);
				System.out.println("AUTH ERROR -> " + fe.toException());
				System.out.println("AUTH ERROR -> " + fe.getMessage());
				System.out.println("AUTH ERROR -> " + fe.getCode());
				Firebase.goOffline();
			}
		};

		try {
			System.out.println("+++++ FIREBASE ++++");
			serviceStream = new FileInputStream(servicePath);
			credential = GoogleCredentials.fromStream(serviceStream);
			//System.out.println("CRED -> " + credential);
			options = FirebaseOptions.builder()
				.setCredentials(credential)
				.setDatabaseUrl(projectUrl)
				.setStorageBucket("hypertheory-fd947.appspot.com")
				.build();

			firestoreOptions = FirestoreOptions.newBuilder()
				.setCredentials(credential)
				.setTimestampsInSnapshotsEnabled(true).build();

			fireApp = FirebaseApp.initializeApp(options);
			System.out.println("FIREBASE APP NAME-> " + fireApp.getName());
			System.out.println("FIREBASE APP OPT -> " + fireApp.getOptions());
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		String uid = "null";
		String customToken = "null";
		String userMail = "erick_cabral@yahoo.com.br";
		String userPass = "#Pass123";
		Map<String, Object> additionalClaims = new HashMap<>();
		additionalClaims.put("simpleAccount", true);

		UserRecord.CreateRequest userInfo = new UserRecord.CreateRequest();
		userInfo.setEmail(userMail);
		userInfo.setPassword(userPass);
		userInfo.setEmailVerified(false);
		userInfo.setDisabled(true);

		FirebaseAuth userAuth = FirebaseAuth.getInstance(fireApp);

		try {
			uid = userAuth.getUserByEmail(userMail).getUid();
			System.out.println("Existent User");
		}
		catch (Exception e) {
			System.out.println("Creating new User");
			try {

				uid = userAuth.createUserAsync(userInfo).get().getUid();
				System.out.println("Enviou o mail ??");
				System.out.println("UID -> " + uid);
			}
			catch (ExecutionException ex) {
				Logger.getLogger(FirebaseApi.class.getName()).log(Level.SEVERE, null, ex);
			}
		}

		UserInfo logRecord = userAuth.getUser(uid);
		System.out.println("User Record -> " + logRecord);
		System.out.println("User UID -> " + logRecord.getUid());
		System.out.println("User mail -> " + logRecord.getEmail());
		System.out.println("User Provider ID -> " + logRecord.getProviderId());

		System.out.println("FIREBASE USER " + FirebaseAuth.getInstance(fireApp).getUser(uid));

		TokenOptions tokenOptions = new TokenOptions();
		tokenOptions.setAdmin(false);
		TokenGenerator tokenGen = new TokenGenerator("SECRET");
		additionalClaims.put("uid", logRecord.getUid());
		additionalClaims.put("email", logRecord.getEmail());
		additionalClaims.put("addinfo", "ADDINFOTESTE");
		customToken = tokenGen.createToken(additionalClaims, tokenOptions);

		firebase = new Firebase(userURL);
		firebase.addAuthStateListener(new Firebase.AuthStateListener() {
			@Override
			public void onAuthStateChanged(AuthData ad) {
				System.out.println(" Auth Listener --- > " + ad);
			}
		});

		//Logando User
		firebase.authWithCustomToken(customToken, authResultHandler);

		Thread.sleep(1000);
		System.out.println("FIREBASE -> " + firebase.getKey());
		System.out.println("FIREBASE -> " + firebase.getApp().toString());
		System.out.println("FIREBASE -> " + firebase.getParent());
		System.out.println("FIREBASE -> " + firebase.getRoot());
		System.out.println("FIREBASE -> " + firebase.getRoot().getKey());

		try {
			firebase.child(userAuth.getUser(uid).getUid()).setValue("NOVO useR");
		}
		catch (FirebaseAuthException e) {
			System.out.println("NAO LOGADO - > " + e);
		}

		Properties userProperties = new Properties();
		userProperties.setProperty("name", "Teste");
		userProperties.setProperty("surname", "Testando");
		userProperties.setProperty("email", "teste@testando.tst");
		userProperties.setProperty("weight", "70");
		userProperties.setProperty("fat", "70");
		userProperties.setProperty("shoulder", "100");
		userProperties.setProperty("chest", "90");
		userProperties.setProperty("waist", "50");
		userProperties.setProperty("glutes", "70");
		userProperties.setProperty("left arm", "20");
		userProperties.setProperty("right arm", "20");
		userProperties.setProperty("left thigh", "40");
		userProperties.setProperty("right thigh", "40");

		firestoreDb = firestoreOptions.getService();
		//dataToInput.put("Teste", "tudo ok!");
		ApiFuture<WriteResult> future;
		CollectionReference otherfuture;
		
		try {
			future = firestoreDb.collection(firebase.getAuth().getUid()).document("user info").collection("Day1").document("initial Info").create(userProperties);
			System.out.println("DATA INSERTED ---> " + future.get().getUpdateTime());
		}
		catch (ExecutionException ex) {
			Logger.getLogger(FirebaseApi.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		firestoreDb.close();

		firebase.child(firebase.getAuth().getUid()).child("inicial Info").setValue(userProperties);

		Bucket mainBuket = StorageClient.getInstance(fireApp).bucket();
		System.out.println("BUCKET -> " + mainBuket.getName());
		System.out.println("BUCKET -> " + mainBuket.getSelfLink());
		System.out.println("BUCKET -> " + mainBuket.getDefaultKmsKeyName());
		System.out.println("BUCKET -> " + mainBuket.getLocation());
		System.out.println("BUCKET -> " + mainBuket.getEtag());

		StorageClient storageClient = StorageClient.getInstance(fireApp);
		InputStream teste = new FileInputStream("F:\\IMAGENS\\Biohazard 640px.jpg");
		String blobb = uid + "/" + "Biohazard 640px.jpg";		
		storageClient.bucket().create(blobb, teste, Bucket.BlobWriteOption.userProject("hypertheory-fd947"));

		firebase.getApp().goOffline();
	}
}
